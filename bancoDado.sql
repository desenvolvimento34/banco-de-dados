/*CREATE DATABASE databank; /*Criação do banco de dados*/
USE databank; /*Acessando database*/
/*Criação de tabelas*/
/*
/*Tabela Cliente*/
CREATE TABLE cliente (
    cliente_cod INT PRIMARY KEY AUTO_INCREMENT
);
/*SELECT * FROM cliente; /*Visualizar table*/
/*DROP TABLE cliente;*/


/*Tabela Pessoa Juridica*/
CREATE TABLE pessoaJuridica (
    pessoaJuridica_cod INT PRIMARY KEY AUTO_INCREMENT
);
SELECT * FROM pessoaJuridica; /*Visualizar table*/

/*Tabela tipo*/
CREATE TABLE tipoDados (
    tipodado_cod INT PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(255)
);
SELECT * FROM tipoDados; /*Visualizar table*/

/*Tabela Pessoa Fisica*/
CREATE TABLE pessoaFisica (
    pessoaFisica_cod INT AUTO_INCREMENT,
    pessoaJuridica_cod INT, /*código da estrangeira*/ 
    nome VARCHAR(255) NOT NULL,
    email VARCHAR(50) NOT NULL,
    cpf  INT NOT NULL, 
    rg INT NOT NULL,
	nascimento DATE NOT NULL,
    PRIMARY KEY (pessoaFisica_cod),
    FOREIGN KEY (pessoaJuridica_cod) REFERENCES pessoaJuridica (pessoaJuridica_cod)
);
SELECT * FROM pessoaFisica;/*Visualizar table*/
DROP TABLE pessoaFisica;

/*Tabela contatos*/
CREATE TABLE contato(
contato_cod INT AUTO_INCREMENT,
tipodado_cod INT,/*código da estrangeira*/ 
nome VARCHAR(255),
campo VARCHAR(255),
PRIMARY KEY (contato_cod),
FOREIGN KEY (tipodado_cod) REFERENCES tipoDados (tipodado_cod)
);
SELECT * FROM contato;
DROP table contato;

/*Inserindo valores na tabela Pessoa Física*/
INSERT INTO pessoaFisica(pessoaFisica_cod,nome,email,cpf,rg,nascimento) VALUES (1,'Yuri Vetoretti', 'yuriVetorreti@gmail.com',1234567891,123456789,'2002-11-25');
INSERT INTO pessoaFisica(pessoaFisica_cod,nome,email,cpf,rg,nascimento) VALUES (2,'Icaro', 'icaro@gmail.com',123456789,987654321,'2003-11-07');
INSERT INTO pessoaFisica(pessoaFisica_cod,nome,email,cpf,rg,nascimento) VALUES (3,'Nicole Padovani', 'nicolePadovani@gmail.com',876536742,768495637,'2001-09-26');

/*Inserindo valores na tabela Pessoa Juridica*/
INSERT INTO pessoaJuridica(pessoaJuridica_cod) VALUES (1);
INSERT INTO pessoaJuridica(pessoaJuridica_cod) VALUES (2);
INSERT INTO pessoaJuridica(pessoaJuridica_cod) VALUES (3);

/*Inserindo valores na tabela Tipo de contato*/
INSERT INTO tipoDados(tipodado_cod,nome) VALUES (1,'Vetoretti');
INSERT INTO tipoDados(tipodado_cod,nome) VALUES (2,'Icaro');
INSERT INTO tipoDados(tipodado_cod,nome) VALUES (3,'Padovani');

/*Inserindo valores na tabela contato*/
INSERT INTO contato(contato_cod,tipodado_cod,nome,campo) VALUES (1,1,'Vetoretti','Enviar contato');
INSERT INTO contato(contato_cod,tipodado_cod,nome,campo) VALUES (2,2,'Icaro','Enviar contato');
INSERT INTO contato(contato_cod,tipodado_cod,nome,campo) VALUES (3,3,'Padovani','Enviar contato');

/*Inserindo valores no cliente*/
INSERT INTO cliente(cliente_cod) VALUES (1);
INSERT INTO cliente(cliente_cod) VALUES (2);
INSERT INTO cliente(cliente_cod) VALUES (3);

